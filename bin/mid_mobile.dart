import 'dart:io';

void main(List<String> arguments) {
  String? input = stdin.readLineSync();
}

List<String> toPostFix(List<String> list) {
  List<String> pf = [];
  List<String> op = [];
  for (var token in list) {
    if (isNumeric(token)) {
      pf.add(token);
    }
    if (isOperator(token)) {
      while (!op.isEmpty &&
          op.last != "(" &&
          procedence(token) <= procedence(op.last)) {
        pf.add(op.removeLast());
      }
      op.add(token);
    }
    if (token == "(") {
      op.add(token);
    }
    if (token == ")") {
      while (op.last != "(") {
        pf.add(op.removeLast());
      }
      op.removeLast();
    }
  }

  while (!op.isEmpty) {
    pf.add(op.removeLast());
  }
  return pf;
}

bool isNumeric(String s) {
  try {
    var val = double.parse(s);
  } on FormatException {
    return false;
  }
  return true;
}

bool isOperator(String s) {
  switch (s) {
    case "+":
    case "-":
    case "*":
    case "/":
      return true;
  }
  return false;
}

int procedence(String s) {
  switch (s) {
    case "+":
      return 1;
    case "-":
      return 1;
    case "*":
      return 2;
    case "/":
      return 2;
    case "^":
      return 3;
    case "(":
      return 4;
    default:
      return -1;
  }
}
